package Pages;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.open;
public class RegistrationPage {

    private final String URL = "https://dev.pandarium.co/#/";

    @FindBy(id = "emailInput")
    private SelenideElement emailInput;

    @FindBy(xpath = "//span[contains(text(), 'Pandopt')]")
    private SelenideElement pandoptButton;

    @FindBy(xpath = "//div[contains(text(), 'Enter your real Email')]")
    private SelenideElement invalidEmailError;

    @FindBy(xpath = "//span[contains(text(), 'Email verification required.')]")
    private SelenideElement emailVerificationMessage;

    @Step("Open registration page")
    public void openRegistrationPage(){
        open(URL);
    }

    @Step("Enter email")
    public void enterEmail(String email){
        emailInput.click();
        emailInput.sendKeys(email);
    }

    @Step("click 'Pandopt' button")
    public void clickPandoptButton(){
        pandoptButton.click();
    }

    @Step("check that error is displayed")
    public boolean invalidEmailMessageIsVisisble(){
        return invalidEmailError.isDisplayed();
    }

    @Step("check that email verification message is visible")
    public boolean emailVerificationMessageIsVisible(){
        emailVerificationMessage.shouldBe(visible);
        return emailVerificationMessage.isDisplayed();
    }
}
