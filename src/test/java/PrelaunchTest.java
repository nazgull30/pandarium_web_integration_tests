import Pages.RegistrationPage;
import com.codeborne.selenide.Configuration;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Title;

import static com.codeborne.selenide.Selenide.page;


public class PrelaunchTest {

    private final String regEmail = "pandarium.test@gmail.com";
    //private final String regEmail = "mitiasizoff@yandex.ru";

    @BeforeClass
    public void setUp(){
        Configuration.browser = "Chrome";
        Configuration.timeout = 10000;
        Configuration.startMaximized = true;
    }

    @Title("Prelaunch user registration test")
    @Test
    public void prelaunchUserRegistrationTest(){
        RegistrationPage registrationPage = page(RegistrationPage.class);

        registrationPage.openRegistrationPage();
        registrationPage.enterEmail(regEmail);
        registrationPage.clickPandoptButton();
        Assert.assertTrue(registrationPage.emailVerificationMessageIsVisible());

    }

    @DataProvider
    public Object [] invalidEmails(){
        return new Object[]{"pandarium.testgmail.com","pandarium.test@gmailcom",
        "@pandarium.test.gmailcom", "pandariu!:m.test@gmailcom"};
    }
    @Title("Email validation test")
    @Test(dataProvider = "invalidEmails")
    public void emailValidationTest(String email){
        RegistrationPage registrationPage = page(RegistrationPage.class);

        registrationPage.openRegistrationPage();
        registrationPage.enterEmail(email);
        registrationPage.clickPandoptButton();
        Assert.assertTrue(registrationPage.invalidEmailMessageIsVisisble());
    }
}
